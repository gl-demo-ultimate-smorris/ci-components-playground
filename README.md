# Akeyless Component Project

This is a component that allows for pulling Akeyless secrets into a job. It completes a few steps:

## What it does
- Installs Akeyless CLI
- Configures Akeyless CLI
- Pulls secret and saves it to MY_SECRET

## What it doesn't do
- It does not use artifacts to store sensitive values
- It does not require the use of the Akeyless base image in the job using the secret
   - Instead, we install the Akeyless CLI directly in the job that uses a different image for other functions

## Assumptions:
- The `ACCESS_ID` from Akeyless is stored as a masked project or group level variable accessible to the `.gitlab-ci.yml` leveraging this component.
- It pulls one secret at a time.
- There is no `before_script` section in the .gitlab-ci.yml job. Since this job is inherited, the CI YAML keys cannot merge their values, so I make the assumption the script section of the `.gitlab-ci.yml` job extending the component is the logic that needs to be preserved.
- The image of the `.gitlab-ci.yml` job, while different from the Akeyless image, has can run `apt-get`.

## How to use this
Any pipeline that needs to pull a secret from Akeyless needs to include the component:

```
include:
  - component: $CI_SERVER_FQDN/gl-demo-ultimate-smorris/components-playground/ci-components-playground/retrieval@X.X.X
    inputs:
      stage: {YOUR_STAGE}
      key: {YOUR_SECRET_NAME}
```

Replace X.X.X with the latest release of the component, and update the stage and key sections. For example:

```
include:
  - component: $CI_SERVER_FQDN/gl-demo-ultimate-smorris/components-playground/ci-components-playground/retrieval@1.1.0
    inputs:
      stage: build
      key: MyFirstSecret
```

In the job itself, simply write `extends: .retrieval` and reference the secret using the MY_SECRET variable. For example:

```
# Use the job from component
extend-retrieval-component:
  image: registry.gitlab.com/gitlab-org/cloud-deploy/aws-base:latest
  extends: .retrieval
  stage: test
  script:
    - echo "This is a job that extends the component"
    - echo $MY_SECRET
```

## How to contribute
Please open an issue and tag @sam, or assign yourself!

##### _Author: @sam / Sam Morris_
-------------------------------
Old README:

This is a component project. The goal of this project is to create a component to retrieve a "secret" that can be used by project pipelines without saving the value as an artifact. This promotes reusability via components and usability of the component at scale.

I want to:

- Create a job that retrieves a value and extend it in a separate pipeline.
- Suppress this component job from running independently, so I do not retrieve the value twice.
- Ensure the value of the variable is usable in my separate project pipeline.

I achieve this through:

- Make the component a hidden job so it does not run just because it is included in the .gitlab-ci.yml file
- Add logic to retrieve key in the `before_script`, so it is merged with the extended job
- Extend the component job and add additional logic in the `script` section that can use the retrieved secret value